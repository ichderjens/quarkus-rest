package restquarkus.data;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.Getter;

import javax.persistence.Entity;

@Data
@Entity
public class Delivery extends PanacheEntity {
    public String name;
}
