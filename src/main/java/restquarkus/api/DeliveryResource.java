package restquarkus.api;

import restquarkus.dto.DeliveryDto;
import restquarkus.mapper.DeliveryMapper;
import restquarkus.service.DeliveryService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/deliveries")
public class DeliveryResource {
    @Inject
    DeliveryService deliveryService;

    @Inject
    DeliveryMapper deliveryMapper;
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<DeliveryDto> getDeliveries() {
        return deliveryMapper.toResource(deliveryService.getDeliveries());
    }
}
